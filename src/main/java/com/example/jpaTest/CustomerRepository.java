/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jpaTest;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author maciek
 */

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    List <Customer> findById(int id);
    List <Customer> findByLastNameContainsOrFirstNameContains(String lastname, String firstname);
}
