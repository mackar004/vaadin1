/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jpaTest;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author maciek
 */
@Entity
@Table(name = "notatka")
public class Notatka {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String tresc;
    private Date data;
    private boolean pozytywna;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id")
    private Customer customer;

    public Notatka() {
    }
    
    public Notatka(String tresc){
        this.tresc = tresc;
    }

    public Notatka(String tresc, Customer customer) {
        this.tresc = tresc;
        this.customer = customer;
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public boolean isPozytywna() {
        return pozytywna;
    }

    public void setPozytywna(boolean pozytywna) {
        this.pozytywna = pozytywna;
    }
    
    public String toString() {
        return this.tresc;
    }

}
