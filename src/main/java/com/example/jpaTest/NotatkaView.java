/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jpaTest;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.springframework.util.StringUtils;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

/**
 *
 * @author maciek
 */

@Route(value = "notatki")
public class NotatkaView extends VerticalLayout{

    private final NotatkaRepository repo;
    final Grid gridNotatka;
    final TextField filtr;
    final NavigationBar menu;

    public NotatkaView(NotatkaRepository repo) {
        this.repo = repo;
        this.gridNotatka = new Grid<>(Notatka.class);
        this.filtr = new TextField();
        this.menu = new NavigationBar();

        filtr.setPlaceholder("Szukaj");
        filtr.setValueChangeMode(ValueChangeMode.EAGER);
        filtr.addValueChangeListener(e -> listNotatka(e.getValue()));

        add(menu, filtr, gridNotatka);
        
        listNotatka(filtr.getValue());
    }

    private void listNotatka(String filtrText) {
        if (StringUtils.isEmpty(filtrText)){
        gridNotatka.setItems(repo.findAll());
        } else {
            gridNotatka.setItems(repo.findByTrescContainsIgnoreCase(filtrText));
        }
    }

}
