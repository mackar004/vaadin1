/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jpaTest;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.springframework.util.StringUtils;

@Route(value = "")
public class MainView extends VerticalLayout {

    private final CustomerRepository repo;
    final Grid grid;
    private final NotatkaRepository notatkaRepo;
    final Grid gridNotatka;
    final TextField filter;
    final NavigationBar menu;
    final CustomerForm customerForm;
    final Dialog dialog;
    private final Button addNewBtn;

    public MainView(CustomerRepository repo, CustomerForm customerForm, NotatkaRepository notatkaRepo) {
        this.repo = repo;
        this.notatkaRepo = notatkaRepo;

        this.grid = new Grid<>(Customer.class);
        this.gridNotatka = new Grid<>(Notatka.class);

        this.filter = new TextField();
        this.menu = new NavigationBar();
        this.customerForm = new CustomerForm(repo);
        this.dialog = new Dialog();

        this.addNewBtn = new Button("New customer", VaadinIcon.PLUS.create());

        HorizontalLayout filterBar = new HorizontalLayout(filter, addNewBtn);

        gridNotatka.setWidth("500px");
        gridNotatka.setColumns("tresc");
        gridNotatka.getColumnByKey("tresc").setHeader("Notatki");

        filter.setPlaceholder("Szukaj w bazie");
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(e -> listCustomers(e.getValue()));

        //dialog.add(new Text("tekst okna"));
        dialog.add(customerForm);
        dialog.add(gridNotatka);
        dialog.setWidth("600px");
        dialog.setHeight("400px");

        grid.setWidth("750px");
        grid.setColumns("id", "lastName", "firstName");
        grid.getColumnByKey("id").setWidth("50px").setFlexGrow(0).setSortProperty("id");

        grid.asSingleSelect().addValueChangeListener(e -> {
            dialog.open();
            listNotatka((Customer) e.getValue());
            Customer test = (Customer)e.getValue();
            customerForm.editCustomer((Customer) e.getValue());
        });

        // Instantiate and edit new Customer the new button is clicked
        addNewBtn.addClickListener(e -> {
            dialog.open();
            customerForm.editCustomer(new Customer("", ""));
        });

        // Listen changes made by the editor, refresh data from backend
        customerForm.setChangeHandler(() -> {
            customerForm.setVisible(false);
            listCustomers(filter.getValue());
            dialog.close();
        });

        add(menu, filterBar, grid);//, customerForm);

        listCustomers(filter.getValue());
    }

    private void listCustomers(String filterText) {
        if (StringUtils.isEmpty(filterText)) {
            grid.setItems(repo.findAll());
        } else {
            grid.setItems(repo.findByLastNameContainsOrFirstNameContains(filterText, filterText));

        }
    }

    private void listNotatka(Customer custo) {
        gridNotatka.setItems(notatkaRepo.findByCustomer(custo));
//        if (StringUtils.isEmpty(filtrText)){
//        gridNotatka.setItems(notatkaRepo.findAll());
//        } else {
//            gridNotatka.setItems(notatkaRepo.findByCustomer(filtrText));
//        }
    }
}
