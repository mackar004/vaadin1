/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jpaTest;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author maciek
 */

public interface NotatkaRepository extends JpaRepository<Notatka, Long> {
    List <Notatka> findById(int id);
    List <Notatka> findByTrescContainsIgnoreCase(String tresc);
    List <Notatka> findByCustomer(Customer customer);
}
