/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jpaTest;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

/**
 *
 * @author maciek
 */

public class NavigationBar extends HorizontalLayout {

    NavigationBar() {
        Button button = new Button("Customers", new Icon(VaadinIcon.USERS));
        button.setHeight("70px");
        button.addClickListener(e -> {
            button.getUI().ifPresent(ui -> ui.navigate(""));
        });
        
        NativeButton button2 = new NativeButton("Notatki");
        button2.addClickListener(e -> {
            button2.getUI().ifPresent(ui -> ui.navigate(button2.getText().toLowerCase()));
        });
        add(button,button2);
    }
}
