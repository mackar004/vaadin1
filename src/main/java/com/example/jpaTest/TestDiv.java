/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jpaTest;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import java.util.Arrays;
import java.util.List;

public class TestDiv extends Div {
    private Grid<Person> grid;
    private Button refresh;

    public TestDiv() {
        this.refresh = new Button("");
        this.refresh.addClickListener(buttonClickEvent -> {
            this.grid.setItems(getPeople());
        });
        this.grid = new Grid<>();
        this.grid.addColumn(Person::getName).setHeader("Name");
        this.grid.addColumn(person -> Integer.toString(person.getYearOfBirth()))
                .setHeader("Year of birth");
        add(this.refresh,this.grid);
    }

    public void update() {
        this.refresh.click();
    }

    private List<Person> getPeople() {
        // Have some data
        List<Person> people = Arrays.asList(
                new Person("Nicolaus Copernicus", 1543),
                new Person("Galileo Galilei", 1564),
                new Person("Johannes Kepler", 1571));
        return people;

    }
}
