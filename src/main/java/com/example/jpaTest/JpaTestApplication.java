package com.example.jpaTest;

import java.util.List;
import java.util.HashSet;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaTestApplication implements CommandLineRunner {
    
    @Autowired
    CustomerRepository customerRepository;
    
    @Autowired
    NotatkaRepository notatkaRepository;

    public static void main(String[] args) {
        SpringApplication.run(JpaTestApplication.class, args);
    }
    
    @Override
    public void run(String... arg0) throws Exception {
    	clearData();
    	saveData();
    	//showData();
    }

    @Transactional
    private void clearData(){
    	customerRepository.deleteAll();
        notatkaRepository.deleteAll();
    }
    
    @Transactional
    private void saveData(){
    	//saveDataWithApproach1();
        saveDataWithApproach2();
    }
    
    @Transactional
    private void showData(){
    	// get All data
    	List<Customer> customerLst = customerRepository.findAll();
        List<Notatka> notatkaLst = notatkaRepository.findAll();
         
        System.out.println("===================Customers:==================");
        customerLst.forEach(System.out::println);
         
        System.out.println("===================Notatki:==================");
        notatkaLst.forEach(System.out::println);
    }

    public void saveDataWithApproach1(){
        Customer jb = new Customer("Jack","Bauer");
        Customer co = new Customer("Chloe","O'Brian");
        
        Notatka n1 = new Notatka("agent pierwszego sortu", jb);
        Notatka n2 = new Notatka("narwaniec", jb);
        
        Notatka n3 = new Notatka("anioł nie kobieta", co);
        Notatka n4 = new Notatka("dobrze, Jack...", co);
        
        jb.setNotatka(new HashSet<Notatka>(){{
            add(n1);
            add(n2);
        }});
        
        co.setNotatka(new HashSet<Notatka>(){{
            add(n3);
            add(n4);
        }});
        
        customerRepository.save(jb);
        customerRepository.save(co);
    }
    
    public void saveDataWithApproach2(){
    	
        Customer jb = new Customer("Jack","Bauer");
        Customer co = new Customer("Chloe","O'Brian");
             
        customerRepository.save(jb);
        customerRepository.save(co);
        
        Notatka n1 = new Notatka("agent pierwszego sortu", jb);
        Notatka n2 = new Notatka("narwaniec", jb);
        
        Notatka n3 = new Notatka("anioł nie kobieta", co);
        Notatka n4 = new Notatka("dobrze, Jack...", co);
 
        notatkaRepository.save(n1);
        notatkaRepository.save(n2);
        
        notatkaRepository.save(n3);
        notatkaRepository.save(n4);
    }
}
